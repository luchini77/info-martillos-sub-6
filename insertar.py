import sqlite3
import os
from crear_tablas import *

class Agregar:

    def __init__(self):

        self.insertar_datos_martillo()
        self.insertar_datos_camara1()
        self.insertar_datos_camara2()

    # DATOS DEL MARTILLO
    def insertar_datos_martillo(self):

        bd = sqlite3.connect('claves_sub-6.db')
        cursor = bd.cursor()

        os.system('clear')
        print('\nIngrese los datos del Martillo.')
        u_martillo = input('\nMartillo:\n>>>')
        num_martillo = input('Numero de Martillo:\n>>>Martillo ')
        firmware = input('Firmware:\n>>>')
        ip_martillo = input('Ip Martillo:\n>>>')
        power_plc = input('Modelo de Power:\n>>>')
        cpu = input('CPU:\n>>>')

        # INSERTANDO DATOS A LA TABLA MARTILLO

        sentencia_martillo = "INSERT INTO martillo(ubicacion, numero, firmware, ip_martillo,power_plc, " \
                         "cpu) VALUES (?,?,?,?,?,?) "

        try:
            cursor.execute(sentencia_martillo, [u_martillo, num_martillo, firmware, ip_martillo, power_plc, cpu])

        except sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:
            bd.commit()
            bd.close()
            print('\nGuardado Correctamente Datos del Martillo')



    def insertar_datos_camara1(self):
        bd = sqlite3.connect('claves_sub-6.db')
        cursor = bd.cursor()

        print('\nIngrese datos de la Camara 1.')
        ip_camara1 = input('\nIP camara 1:\n>>>')
        modelo_cam1 = input('Modelo de Camara:\n>>>')
        usuario_cam1 = input('Nombre Usuario:\n>>>')
        pass_cam1 = input('Password:\n>>>')
        u_mart1 = input('A que martillo pertenece la camara:\n>>>')

        # INSERTANDO DATOS A LA TABLA CAMARA 1

        sentencia_camara1 = "INSERT INTO camara1(ip_camara1, modelo_cam1, usuario_cam1, pass_cam1, ubicacion_cam1) VALUES (?,?,?,?,?)"

        try:
            cursor.execute(sentencia_camara1, [ip_camara1, modelo_cam1, usuario_cam1, pass_cam1, u_mart1])

        except sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:
            bd.commit()
            bd.close()
            print('\nGuardado Correctamente Datos de la Camara 1')


    def insertar_datos_camara2(self):
        bd = sqlite3.connect('claves_sub-6.db')
        cursor = bd.cursor()

        print('\nIngrese datos de la Camara 2.')
        ip_camara2 = input('\nIP camara 2:\n>>>')
        modelo_cam2 = input('Modelo de Camara:\n>>>')
        usuario_cam2 = input('Nombre Usuario:\n>>>')
        pass_cam2 = input('Password:\n>>>')
        u_mart2 = input('A que martillo pertenece la camara:\n>>>')


        # INSERTANDO DATOS A LA TABLA CAMARA 2
        sentencia_camara2 = "INSERT INTO camara2(ip_camara2, modelo_cam2, usuario_cam2, pass_cam2, ubicacion_cam2) VALUES (?,?,?,?,?)"

        try:
            cursor.execute(sentencia_camara2, [ip_camara2, modelo_cam2, usuario_cam2, pass_cam2, u_mart2])

        except sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:
            bd.commit()
            bd.close()
            print('\nGuardado Correctamente Datos de la Camara 2')
            print(input('\n\tPresiona Enter para continuar!!!!'))
            os.system('clear')


#Agregar()
