import sqlite3

class CrearTabla:

    def __init__(self):
        self.bd = sqlite3.connect('claves_sub-6.db')
        self.cursor = self.bd.cursor()

        self.tabla_martillo()
        self.tabla_camara1()
        self.tabla_camara2()


    def tabla_martillo(self):
        tablas = [
            """
            CREATE TABLE IF NOT EXISTS martillo(
            ubicacion TEXT NOT NULL PRIMARY KEY UNIQUE,
            numero TEXT,
            firmware TEXT,
            ip_martillo TEXT NOT NULL UNIQUE,
            power_plc TEXT,
            cpu TEXT
            );
            """
        ]
        try:
            for tabla in tablas:
                self.cursor.execute(tabla)
                print('Tabla Martillo creada correctamente')
        except  sqlite3.OperationalError as e:
            print('Error al abrir: ', e)


    def tabla_camara1(self):
            tablas = [
                """
                CREATE TABLE IF NOT EXISTS camara1(
                ip_camara1 TEXT NOT NULL PRIMARY KEY UNIQUE,
                modelo_cam1 TEXT,
                usuario_cam1 TEXT,
                pass_cam1 TEXT NOT NULL UNIQUE,
                ubicacion_cam1 TEXT,
                FOREIGN KEY ('ubicacion_cam1') REFERENCES martillo('ubicacion')
                );
                """
            ]
            try:
                for tabla in tablas:
                    self.cursor.execute(tabla)
                    print('Tabla Camara 1 creada correctamente')
            except  sqlite3.OperationalError as e:
                print('Error al abrir: ', e)


    def tabla_camara2(self):
            tablas = [
                """
                CREATE TABLE IF NOT EXISTS camara2(
                ip_camara2 TEXT NOT NULL PRIMARY KEY UNIQUE,
                modelo_cam2 TEXT,
                usuario_cam2 TEXT,
                pass_cam2 TEXT NOT NULL UNIQUE,
                ubicacion_cam2 TEXT,
                FOREIGN KEY ('ubicacion_cam2') REFERENCES martillo('ubicacion')
                );
                """
            ]
            try:
                for tabla in tablas:
                    self.cursor.execute(tabla)
                    print('Tabla Camara 2 creada correctamente')
            except  sqlite3.OperationalError as e:
                print('Error al abrir: ', e)



#CrearTabla()
