import getpass
import os
from insertar import Agregar
from consultar import Leer
from eliminar import Borrar


class Menu:

    def __init__(self):


        self.principal()

    def validar_pass(self,password):
        return self.password == 'grupo1'


    def principal(self):

        usuario = 'James'

        print('\n\t***DEBES ESTAR REGISTRADO PARA INGRESAR, INTRODUCE TU CONTRASEÑA:***\n')

        self.password = getpass.getpass()

        if self.validar_pass(self.password):
            print(f'\n\tOli {usuario}')
            print('\n\tBienvenido a la BBDD del Sub-6')
            print(input('\n\tPresiona Enter para continuar!!!!'))
            self.menu()
        else:
            print('\n\tContraseña Incorrecta')
            exit()


    def menu(self):
        os.system('clear')

        while True:
            print('\n\t************************************************')
            print('\tBienvenido a la aplicacion Claves del Sub-6')
            print('\t************************************************\n')
            print('1 - Insertar Datos')
            print('2 - Consultar Datos')
            print('3 - Eliminar Datos')
            print('9 - Salir')

            opc = input('\nIntrocuce una opcion: \n>>>')
            if not opc:
                print('\nNo escribio nada\n')
                print('Vuelva a intentarlo\n')
                menu()

            if opc == '1':
                os.system('clear')
                Agregar()

            elif opc == '2':
                os.system('clear')
                #self.martillo = input('Ingrese el martillo a consultar:\n>>> ')
                Leer()

            elif opc == '3':
                os.system('clear')
                Borrar()

            elif opc == '9':

                os.system('clear')
                print('\n\t===================================')
                print('\tGracias por utilizar la aplicacion.')
                print('\t===================================\n')
                exit()

            else:
                os.system('clear')
                print('\n\t===================================')
                print('\tEsa opcion no existe, intentelo de nuevo.')
                print('\t===================================\n')

                op = input('\nVolver al menu presione S, si desea salir de la aplicacion presione N .........\n>>>')
                if op.lower() == 's':
                    self.menu()
                else:
                    os.system('clear')
                    print('\n\t===================================')
                    print('\tGracias por utilizar la aplicacion.')
                    print('\t===================================\n')
                    exit()


if __name__ == '__main__':
    Menu()

#usu = Menu()
#usu.principal()
