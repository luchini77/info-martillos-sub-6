from tabulate import tabulate
import sqlite3
import os

class Leer:

    def __init__(self):

        self.consulta_uno()

    # DATOS DE LOS MARTILLOS Y RESPECTIVAS CAMARAS
    def consulta_uno(self):

        martillo = input('Ingrese martillo a buscar:\n>>> ')


        # DATOS DE LA CONSULTA DE MARTILLO
        sentencia_martillo = "SELECT * FROM martillo WHERE ubicacion = ?"

        try:

            bd = sqlite3.connect('claves_sub-6.db')
            cursor = bd.cursor()

            cursor.execute(sentencia_martillo, [martillo])


        except sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:
            print('\nInformacion del Martillo.\n')

            print(tabulate(cursor,
                        headers=['Martillo', 'Numero Martillo', 'Firmware', 'IP Martillo', 'Power', 'CPU'],
                        tablefmt='grid'))

        # DATOS DE LA CONSULTA DE CAMARA 1
        sentencia_camara1 = "SELECT ubicacion_cam1, ip_camara1, modelo_cam1,usuario_cam1, pass_cam1 FROM camara1 WHERE ubicacion_cam1 = ?"

        try:
            cursor.execute(sentencia_camara1, [martillo])

        except sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:
            print('\nInformacion de la Camara 1.\n')

            print(
                tabulate(cursor, headers=['Pertenece a', 'Ip Camara', 'Modelo de Camara', 'Usuario', 'Password', ],
                        tablefmt='grid'))

        # DATOS DE LA CONSULTA DE CAMARA 2
        sentencia_camara2 = "SELECT ubicacion_cam2, ip_camara2, modelo_cam2,usuario_cam2, pass_cam2 FROM camara2 WHERE ubicacion_cam2 = ?"

        try:
            cursor.execute(sentencia_camara2, [martillo])

        except sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:
            print('\nInformacion de la Camara 2.\n')

            print(
                tabulate(cursor, headers=['Pertenece a', 'Ip Camara', 'Modelo de Camara', 'Usuario', 'Password', ],
                        tablefmt='grid'))

            print('')
            bd.close()
