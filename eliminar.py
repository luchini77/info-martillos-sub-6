import sqlite3
import  os

class Borrar:

    def __init__(self):

        self.eliminar()

    def eliminar(self):

        martillo = input('Ingrese martillo a buscar:\n>>> ')

        #SENTENCIA PARA ELIMIRAR CAMARA 1
        sentencia_camara1 = "DELETE FROM camara1 WHERE ubicacion_cam1 = ?"

        try:

            bd = sqlite3.connect('claves_sub-6.db')
            cursor = bd.cursor()

            cursor.execute(sentencia_camara1, [martillo])

        except  sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:

            bd.commit()

        #SENTENCIA PARA ELIMINAR CAMARA 2
        sentencia_camara2 = "DELETE FROM camara2 WHERE ubicacion_cam2 = ?"

        try:

            bd = sqlite3.connect('claves_sub-6.db')
            cursor = bd.cursor()

            cursor.execute(sentencia_camara2, [martillo])

        except  sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:

            bd.commit()

        #SENTENCIA PARA ELIMINAR MARTILLO
        sentencia_martillo = "DELETE FROM martillo WHERE ubicacion = ?"

        try:
            bd = sqlite3.connect('claves_sub-6.db')
            cursor = bd.cursor()

            cursor.execute(sentencia_martillo, [martillo])

        except sqlite3.OperationalError as error:
            print('Error al abrir:', error)

        else:
            bd.commit()
            bd.close()
            os.system('clear')
            print(f'Eliminado el {martillo}')
            print(input('\n\tPresiona Enter para continuar!!!!'))
            os.system('clear')
